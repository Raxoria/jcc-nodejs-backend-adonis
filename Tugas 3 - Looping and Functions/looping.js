//Soal 1
console.log('Looping Pertama');
var jumlah = 0;
while (jumlah < 20){
	jumlah = jumlah + 2;
	console.log(jumlah + ' - I love coding');
}
console.log('Looping Kedua');
while (jumlah > 0){
	console.log(jumlah + ' - I will become a mobile developer');
	jumlah = jumlah - 2;
}

//Soal 2  
for (var angka = 1; angka <= 20; angka++){
	if (angka%3 == 0 && angka%2 != 0){
		console.log(angka + ' - I Love Coding');
	}	
	else if (angka%2 != 0){
		console.log(angka + ' - Santai');
	}
	else if (angka%2 == 0){
		console.log(angka + ' - Berkualitas');
	}
	
}

//Soal 3
var args = process.argv;
function persegiPanjang(panjang, lebar){
	while(lebar>0){
		for(angka=0;angka<panjang; angka++){
			process.stdout.write("#");
		}
		lebar--;
		process.stdout.write("\n");
	}
}
persegiPanjang(args[3], args[4]);

//Soal 4
var args = process.argv;
function tangga(langkah){
	for(var angka = 1; angka<=langkah; angka++){
		for(var angka2 = 1; angka2<=angka; angka2++){
		process.stdout.write("#");
		}process.stdout.write("\n");
	}
}
tangga(args[3]);

//Soal 5
var args = process.argv;
function catur(kotak){
	for(var angkaa = 1; angkaa<=kotak; angkaa++){
		for(var angkaa2 = 1; angkaa2<=kotak; angkaa2++){
			if(angkaa % 2 == 0){
				if(angkaa2 % 2 != 0){
				process.stdout.write(" ");
				}else{
				process.stdout.write("#");
				}				
			}else{ 
				if(angkaa2 % 2 == 0){
				process.stdout.write(" ");
				}else{
				process.stdout.write("#");
				}	
			}
		}process.stdout.write("\n");
	}
}
catur(args[3]);