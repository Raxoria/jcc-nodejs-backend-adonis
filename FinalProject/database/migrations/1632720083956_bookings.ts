import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class bookings extends BaseSchema {
  protected tableName = 'bookings'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('field_id').unsigned().references('id').inTable('fields').onUpdate('CASCADE').onDelete('CASCADE');
      table.timestamp('play_date_start').nullable();
      table.timestamp('play_date_finish').nullable();
      table.integer('booking_user_id').unsigned().references('id').inTable('users').onUpdate('CASCADE').onDelete('CASCADE');

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true }).defaultTo(this.now())
      table.timestamp('updated_at', { useTz: true }).defaultTo(this.now())
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
