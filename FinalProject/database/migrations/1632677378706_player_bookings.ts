import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class PlayerBookings extends BaseSchema {
  protected tableName = 'player_bookings'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.integer('booking_id').unsigned().references('id').inTable('bookings').onDelete('CASCADE').onUpdate('CASCADE');
      table.integer('booking_user_id').unsigned().references('id').inTable('users').onUpdate('CASCADE').onDelete('CASCADE');

      table.timestamp('expires_at', { useTz: true }).nullable()
      table.timestamp('created_at', { useTz: true }).notNullable().defaultTo(this.now())
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}