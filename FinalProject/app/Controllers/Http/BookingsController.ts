import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Booking from 'App/Models/Booking'
import Field from 'App/Models/Field'
import User from 'App/Models/User'
import Database from "@ioc:Adonis/Lucid/Database";
import BookingValidator from 'App/Validators/BookingValidator'

export default class BookingsController {
  public async index ({response}: HttpContextContract) {
    let bookings = await Booking.query()
    return response.status(200).json({message: 'success get bookings data', data: bookings})
  }

  // public async store ({request, response, params, auth}: HttpContextContract) {
  //   try{
  //     const field = await Field.findByOrFail('id', params.field_id)
  //     const userAuth = auth.user!
  //     const payload = await request.validate(BookingValidator)
  //     const booking = new Booking()
  //     booking.playDateStart = payload.play_date_start
  //     booking.playDateFinish = payload.play_date_finish
  //     booking.related('field').associate(field)
  //     booking.related('bookUser').associate(userAuth)
  //     response.created({ status : 'success', message : `Data berhasil ditambahkan`, data : booking })
  //   } catch (error) {
  //     response.badRequest({ errors : error.message, message: 'Data gagal ditambah', validator: error.messages})
  //   }
  // }

  public async books ({response,request , auth, params}: HttpContextContract) {
    try {
      const field = await Field.findByOrFail('id', params.id)
      const user = auth.user!
      console.log(User)
      await request.validate(BookingValidator) 
      
      //Lucid Orm
      const newBooking = new Booking();
      newBooking.playDateStart = request.input('play_date_start') 
      newBooking.playDateFinish = request.input('play_date_finish')
      newBooking.related('field').associate(field)
      user.related('myBooking').save(newBooking)
      
      return response.status(200).json({message: 'berhasil booking', data: newBooking})
  } catch (error) {
      response.unprocessableEntity({message: error.message})
    }
  }
  public async join({response, auth, params}:HttpContextContract){
    const booking = await Booking.findOrFail(params.id)
    let user = auth.user!
    const checkJoin = await Database.from('users_has_bookings').where('booking_id', params.id).where('user_id', user.id).first()

    if (!checkJoin) {
        await booking.related('players').attach([user.id])
    } else {
        await booking.related('players').detach([user.id])
    }

    return response.ok({message: 'success', data: 'successfully join/unjoin'})
}
  public async show ({}: HttpContextContract) {    
  }

  public async edit ({}: HttpContextContract) {
  }

  public async update ({}: HttpContextContract) {
  }

  public async destroy ({}: HttpContextContract) {
  }
}
