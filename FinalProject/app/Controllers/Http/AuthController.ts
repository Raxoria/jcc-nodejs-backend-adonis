import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import UserValidator from 'App/Validators/UserValidator'
import User from 'App/Models/User'
import { schema } from '@ioc:Adonis/Core/Validator'

export default class UsersController {
    public static async store({ request, response } : HttpContextContract){
        try {
            const data = await request.validate(UserValidator)
            await User.create(data)
            return response.created({ message : "Register Berhasil!" })
        } catch (error) {
            return response.unprocessableEntity({ message : error.message, error})
        }
    }

    public static async login({ request, response, auth } : HttpContextContract) {
        const loginUser = schema.create({
            email: schema.string(),
            password : schema.string()
        })
        try {
            const payload = await request.validate({ schema : loginUser })
            const { email, password } = payload
            const token = await auth.use('api').attempt(email, password)
            return response.ok({ message : "login berhasil ", token})
        } catch(error){
            if(error.guard) {
                return response.badRequest({ message: 'Login Gagal', error : error.message })
            } else {
                return response.badRequest({ message: 'Login Gagal', error : error.messages })
            }
        }
    } 
}
