import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import VenueValidator from "App/Validators/VenueValidator";
//import Database from "@ioc:Adonis/Lucid/Database";
import Venue from "App/Models/Venue";

export default class VenuesController {

  public async index ({ response }: HttpContextContract) {
    const data = await Venue.all();

    return response.status(200).json({
      response_message: "Data Fetched",
      data
    });
  }

  public async show ({response, params}: HttpContextContract) {
    try {
        let id = params.id;

        const data = await Venue.firstOrFail();

        return response.status(200).json({
        response_message: `Data Fetched`,
        data
        });
    }catch (errors) {
        return response.status(404).json({
            response_message: `Data Not Found`,
            error_message: errors.message
          }); 
    }
  }

  public async store ({request, response, auth}: HttpContextContract) {
    try {
      const data = await request.validate(VenueValidator)
      const newData = await Venue.create(data)
      const userId = auth.user?.id
      console.log('user id : ', userId)
      return response.created({ status : 'success', message : `Add Data Success`, data : newData })
  } catch (error) {
      return response.unprocessableEntity({ error: 'Failed to Add Data', message : error.messages })
  }
}

  public async update ({request, response, params}: HttpContextContract) {
    const id = params.id;
    let name = request.input('name');
    let address = request.input('address');
    let phone = request.input('phone');

    await request.validate(VenueValidator);

    try{

    const data = await Venue.firstOrFail();
        data['name'] = name;
        data['address'] = address;
        data['phone'] = phone;

        await data.save();

        return response.status(200).json({
         response_message: `Data Updated`,
        data
        });
    }catch (errors) {
        return response.status(404).json({
            response_message: `Data Not Found`,
            error_message: errors.message
          }); 
    }
  }

  public async destroy ({response, params}: HttpContextContract) {
    const id = params.id;

    try {
        const venue = await Venue.findOrFail(id);
        await venue.delete();
  
        return response.status(201).json({
          response_message: `Data Successfully Deleted`
        });
      } catch (errors) {
        return response.status(404).json({
            response_message: `Data Not Found`,
            error_message: errors.message
        }); 
      }
  }
} 