import { schema, rules } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class UserValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
	  full_name: schema.string({}, [
		  rules.minLength(6)
	  ]),
	  email: schema.string({},[
		  rules.email(),
		  rules.unique({
			  table: 'users',
			  column: 'email'
		  })
	  ]),
	  password: schema.string({}, [
		  rules.minLength(3)
	  ]),
	  role: schema.enum([
		'user', 'owner'
	  ] as const),
	})

	public messages = {
		'required' : '{{field}} Wajib Diisi',
		'alpha' : '{{field}} Harus Diisi Karakter',
		'mobile' : '{{field}} Harus Diawali dengan +62',
		'enum' : "{{field}} Harus Termasuk 'User'/'Owner'",
		'unique' : "{{field}} harus unik, tidak boleh ada yang sama"
	}
}
