import {Bootcamp} from './lib/bootcamp'
import {Student} from './lib/student'


//Release 0
const sanber = new Bootcamp("sanbercode")
sanber.createClass("Laravel", "beginner", "abduh")
sanber.createClass("React", "beginner", "abdul")
console.log(sanber.classes)

//Release 1
let names = ["regi", "ahmad", "bondra", "iqbal", "putri", "rezky"]
names.map((nama, index) => {
  let newStud = new Student(nama)
  let kelas = sanber.classes[index % 2].name
  sanber.register(kelas, newStud)
})
// menampilkan data kelas dan student nya
sanber.classes.forEach(kelas => {
  console.log(kelas)
});

//Release 2
sanber.classes.forEach(kelas => {
    console.log(kelas.graduate())
  });

//Release 3
sanber.runBatch();
