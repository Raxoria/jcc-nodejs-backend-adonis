import {Student} from './student'

export class Kelas{
    constructor(nama,level,instructor){
        this._nama = nama
        this._students = []
        this._level = level
        this._instructor = instructor
    }
    get name(){
        return this._nama
    }
    set name(nama){
        this._nama = nama
    }
    get level(){
        return this._level
    }
    set level(level){
        this._level = level
    }
    get instructor(){
        return this._instructor
    }
    set instructor(instructor){
        this._instructor = instructor
    }
    get students(){
        return this._students
    }
    set students(students){
        this._students = students
    }
    graduate(){
        let objGraduate = {
            participated: [],
            completed: [],
            master: []
        }
        for(let i=0; i<this._students.length; i++){
            if (this._students[i].finalScore < 60){
                objGraduate.participated.push(this._students[i])
            }if (this._students[i].finalScore >= 60 && this._students[i].finalScore <= 85){
                objGraduate.completed.push(this._students[i])
            }if (this._students[i].finalScore > 85){
                objGraduate.master.push(this._students[i])
            }        
        }
        return objGraduate;
    }
}