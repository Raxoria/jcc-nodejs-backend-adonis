import {Kelas} from './kelas'
import {Student} from './student'

export class Bootcamp{
    constructor(name){
        this._name = name
        this._classes = []
    }
    get names(){
        return this._names
    }
    set name(name){
        this._name = name;
    }
    get classes(){
        return this._classes
    }
    set classes(classes){
        this._classes = classes;
    }

    createClass(nama,level,instructor){
        this._classes.push(new Kelas(nama,level,instructor))
    }

    register(kelas, newstud){
        for (var i = 0; i < this._classes.length; i++){
            if(this._classes[i].name == kelas){
                let temp = this._classes[i].students // get
                temp.push(newstud) // tambah student
                this._classes[i].students = temp // set students
                break
            }
        }
    }

    runBatch(){
        for(let i=0; i<4; i++){
            this._classes.forEach(kelas => {
                kelas.students.forEach(siswa => {
                    siswa.addScore(Math.floor(Math.random() * 51) + 50)
                })
            })
        } 
        this._classes.forEach(kelas => {
            console.log("Graduate from ", kelas.name , kelas.graduate())
        });
    }
}