export class Student{
    constructor(name){
        this._name = name
        this._score = []
        this._finalScore = 0
    }
    get finalScore(){
        return this._finalScore
    }
    addScore(x){
        this._score.push(x)
        let sum = 0
        this._score.forEach(num => {
            sum += num
        })
        this._finalScore = sum / this._score.length
    }   
    // get name(){
    //     return this._name
    // }
    // get score(){
    //     return this._score
    // }
}