"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Bootcamp = void 0;

var _kelas = require("./kelas");

var _student = require("./student");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Bootcamp = /*#__PURE__*/function () {
  function Bootcamp(name) {
    _classCallCheck(this, Bootcamp);

    this._name = name;
    this._classes = [];
  }

  _createClass(Bootcamp, [{
    key: "names",
    get: function get() {
      return this._names;
    }
  }, {
    key: "name",
    set: function set(name) {
      this._name = name;
    }
  }, {
    key: "classes",
    get: function get() {
      return this._classes;
    },
    set: function set(classes) {
      this._classes = classes;
    }
  }, {
    key: "createClass",
    value: function createClass(nama, level, instructor) {
      this._classes.push(new _kelas.Kelas(nama, level, instructor));
    }
  }, {
    key: "register",
    value: function register(kelas, newstud) {
      for (var i = 0; i < this._classes.length; i++) {
        if (this._classes[i].name == kelas) {
          var temp = this._classes[i].students; // get

          temp.push(newstud); // tambah student

          this._classes[i].students = temp; // set students

          break;
        }
      }
    }
  }, {
    key: "runBatch",
    value: function runBatch() {
      for (var i = 0; i < 4; i++) {
        this._classes.forEach(function (kelas) {
          kelas.students.forEach(function (siswa) {
            siswa.addScore(Math.floor(Math.random() * 51) + 50);
          });
        });
      }

      this._classes.forEach(function (kelas) {
        console.log("Graduate from ", kelas.name, kelas.graduate());
      });
    }
  }]);

  return Bootcamp;
}();

exports.Bootcamp = Bootcamp;