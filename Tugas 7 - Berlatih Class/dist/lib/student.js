"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Student = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Student = /*#__PURE__*/function () {
  function Student(name) {
    _classCallCheck(this, Student);

    this._name = name;
    this._score = [];
    this._finalScore = 0;
  }

  _createClass(Student, [{
    key: "finalScore",
    get: function get() {
      return this._finalScore;
    }
  }, {
    key: "addScore",
    value: function addScore(x) {
      this._score.push(x);

      var sum = 0;

      this._score.forEach(function (num) {
        sum += num;
      });

      this._finalScore = sum / this._score.length;
    } // get name(){
    //     return this._name
    // }
    // get score(){
    //     return this._score
    // }

  }]);

  return Student;
}();

exports.Student = Student;