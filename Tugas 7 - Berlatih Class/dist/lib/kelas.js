"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Kelas = void 0;

var _student = require("./student");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Kelas = /*#__PURE__*/function () {
  function Kelas(nama, level, instructor) {
    _classCallCheck(this, Kelas);

    this._nama = nama;
    this._students = [];
    this._level = level;
    this._instructor = instructor;
  }

  _createClass(Kelas, [{
    key: "name",
    get: function get() {
      return this._nama;
    },
    set: function set(nama) {
      this._nama = nama;
    }
  }, {
    key: "level",
    get: function get() {
      return this._level;
    },
    set: function set(level) {
      this._level = level;
    }
  }, {
    key: "instructor",
    get: function get() {
      return this._instructor;
    },
    set: function set(instructor) {
      this._instructor = instructor;
    }
  }, {
    key: "students",
    get: function get() {
      return this._students;
    },
    set: function set(students) {
      this._students = students;
    }
  }, {
    key: "graduate",
    value: function graduate() {
      var objGraduate = {
        participated: [],
        completed: [],
        master: []
      };

      for (var i = 0; i < this._students.length; i++) {
        if (this._students[i].finalScore < 60) {
          objGraduate.participated.push(this._students[i]);
        }

        if (this._students[i].finalScore >= 60 && this._students[i].finalScore <= 85) {
          objGraduate.completed.push(this._students[i]);
        }

        if (this._students[i].finalScore > 85) {
          objGraduate.master.push(this._students[i]);
        }
      }

      return objGraduate;
    }
  }]);

  return Kelas;
}();

exports.Kelas = Kelas;