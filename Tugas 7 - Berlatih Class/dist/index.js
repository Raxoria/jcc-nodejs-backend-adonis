"use strict";

var _bootcamp = require("./lib/bootcamp");

var _student = require("./lib/student");

//Release 0
var sanber = new _bootcamp.Bootcamp("sanbercode");
sanber.createClass("Laravel", "beginner", "abduh");
sanber.createClass("React", "beginner", "abdul");
console.log(sanber.classes); //Release 1

var names = ["regi", "ahmad", "bondra", "iqbal", "putri", "rezky"];
names.map(function (nama, index) {
  var newStud = new _student.Student(nama);
  var kelas = sanber.classes[index % 2].name;
  sanber.register(kelas, newStud);
}); // menampilkan data kelas dan student nya

sanber.classes.forEach(function (kelas) {
  console.log(kelas);
}); //Release 2

sanber.classes.forEach(function (kelas) {
  console.log(kelas.graduate());
}); //Release 3

sanber.runBatch();