//Soal 1
function arrayToObject(arr = undefined){
    var now = new Date()
    var thisYear = now.getFullYear()
    var hasil = {
        firstName : "",
        lastName : "",
        gender : "",
        age : "Invalid Birth Day"
    };
    var j = 1;
    for(var n = 0; n<arr.length; n++){
        for(var m =0; m<4;m++){ //m<arr[n].length?
            switch(m){
            case 0:hasil.firstName = arr[n][m]; break;
            case 1:hasil.lastName = arr[n][m];break;
            case 2:hasil.gender = arr[n][m];break;
            case 3: 
                if (arr[n].length === 3 || thisYear-arr[n][m]<=0 || arr[n][m] === undefined){
                hasil.age = "Invalid Birth Day"; 
                break;}
                hasil.age = thisYear - arr[n][m]; 
                break;
            };
        }
        console.log(j+". "+ hasil.firstName +" "+ hasil.lastName+ ": " +  `{"firstName": '${hasil.firstName}', "lastName": '${hasil.lastName}', "gender": '${hasil.gender}', "age": ${hasil.age}}`);
        j++;
    } 
    console.log("");
}
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2);
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

//Soal 2
function shoppingTime(memberId, money){
    if (memberId === undefined || memberId.length === 0){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }else if(money<50000){
        return "Mohon maaf, uang tidak cukup";
    }
    var customer = {
        memberId : memberId,
        money : money,
        listPurchased : [],
        changeMoney : 0
    };
    if (money-customer.changeMoney >= 1500000){
        customer.listPurchased.push("Sepatu Stacattu");
        customer.changeMoney = customer.changeMoney + 1500000
    }
    if (money-customer.changeMoney >= 500000){
        customer.listPurchased.push("Baju Zoro");
        customer.changeMoney = customer.changeMoney + 500000
    }
    if (money-customer.changeMoney >= 250000){
        customer.listPurchased.push("Baju H&N");
        customer.changeMoney = customer.changeMoney + 250000
    }
    if (money-customer.changeMoney >= 175000){
        customer.listPurchased.push("Sweater Uniklooh");
        customer.changeMoney = customer.changeMoney + 175000
    }
    if (money-customer.changeMoney >= 50000){
        customer.listPurchased.push("Casing Handphone");
        customer.changeMoney = customer.changeMoney + 50000
    }
    customer.changeMoney = money - customer.changeMoney
    return customer;
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//Soal 3
function naikAngkot(arrpenumpang){
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var slot = [{
        penumpang : "",
        naikDari : "",
        tujuan: "",
        ongkos: 0
    },{
        penumpang : "",
        naikDari : "",
        tujuan: "",
        ongkos: 0
    }]
    if (arrpenumpang.length === 0 || arrpenumpang === undefined){
        return arrpenumpang;
    }
    for(var x = 0; x<arrpenumpang.length;x++){
        for(var y = 0; y<3;y++){
            if (y === 0){slot[x].penumpang = arrpenumpang[x][y]}
            else if (y === 1){slot[x].naikDari = arrpenumpang[x][y]}
            else if (y === 2){slot[x].tujuan = arrpenumpang[x][y]}   
        }
        for(let z = 0; z<rute.length;z++){
            if(slot[x].naikDari === rute[z]){
                var s1 = z + 1;
            }
            if(slot[x].tujuan === rute[z]){
                var s2 = z + 1;
            }
        }
        var selisih = s1 - s2;
        if(selisih <0){
            selisih = selisih*-1;
        }
        slot[x].ongkos = selisih * 2000;
    }
    return slot;
}
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
console.log(naikAngkot([])); //[]

//Soal 4
function found(data, inputan){
    let s;
    for(s=0; s<data.length; s++){
        if(data[s]===inputan){
            return true;
        }
    }
    return false;
}
function nilaiTertinggi(siswa) {
    var nama = [];
    var kelas = [];
    var nilai = [0,0,0];
    let p,q;
    if(siswa.length === 0 || siswa === undefined){
        return siswa;
    }
    for(p=0; p<siswa.length;p++){
        if(found(kelas, siswa[p].class)){

        }else{
        kelas.push(siswa[p].class)}
    }
    for(p=0; p<kelas.length; p++){
        for(q=0; q<siswa.length;q++){
            if(kelas[p]=== siswa[q].class){
                if(nilai[p]<siswa[q].score){
                    nilai[p] = siswa[q].score;
                    nama[p]= siswa[q].name;
                }
            }
        }
    }
    console.log("{");
    console.log(`   ${kelas[0]}: { name: '${nama[0]}', score: ${nilai[0]} }`);
    console.log(`   ${kelas[1]}: { name: '${nama[1]}', score: ${nilai[1]} }`);
    console.log(`   ${kelas[2]}: { name: '${nama[2]}', score: ${nilai[2]} }`);
    console.log("}");
  }
  
  // TEST CASE
  console.log(nilaiTertinggi([
    {
      name: 'Asep',
      score: 90,
      class: 'adonis'
    },
    {
      name: 'Ahmad',
      score: 85,
      class: 'vuejs'
    },
    {
      name: 'Regi',
      score: 74,
      class: 'adonis'
    },
    {
      name: 'Afrida',
      score: 78,
      class: 'reactjs'
    }
  ]));
  
  // OUTPUT:
  
  // {
  //   adonis: { name: 'Asep', score: 90 },
  //   vuejs: { name: 'Ahmad', score: 85 },
  //   reactjs: { name: 'Afrida', score: 78}
  // }
  
  
  console.log(nilaiTertinggi([
    {
      name: 'Bondra',
      score: 100,
      class: 'adonis'
    },
    {
      name: 'Putri',
      score: 76,
      class: 'laravel'
    },
    {
      name: 'Iqbal',
      score: 92,
      class: 'adonis'
    },
    {
      name: 'Tyar',
      score: 71,
      class: 'laravel'
    },
    {
      name: 'Hilmy',
      score: 80,
      class: 'vuejs'
    }
  ]));
  
  // {
  //   adonis: { name: 'Bondra', score: 100 },
  //   laravel: { name: 'Putri', score: 76 },
  //   vuejs: { name: 'Hilmy', score: 80 }
  // }