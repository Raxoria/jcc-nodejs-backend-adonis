"use strict";

var _fsfunc = require("./lib/fsfunc");

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var args = process.argv.slice(2);
var command = args[0];

switch (command) {
  case "register":
    var newData = {};

    var _args$1$split = args[1].split(','),
        _args$1$split2 = _slicedToArray(_args$1$split, 3),
        nama = _args$1$split2[0],
        password = _args$1$split2[1],
        role = _args$1$split2[2];

    newData.nama = nama;
    newData.role = role;
    newData.pass = password;
    newData.isLogin = false;
    (0, _fsfunc.register)(newData);
    break;

  case "login":
    var log = {};

    var _args$1$split3 = args[1].split(','),
        _args$1$split4 = _slicedToArray(_args$1$split3, 2),
        id = _args$1$split4[0],
        pass = _args$1$split4[1];

    log.nama = id;
    log.pass = pass;
    (0, _fsfunc.login)(log);
    break;

  case "addSiswa":
    var add = {};

    var _args$1$split5 = args[1].split(','),
        _args$1$split6 = _slicedToArray(_args$1$split5, 2),
        name1 = _args$1$split6[0],
        name2 = _args$1$split6[1];

    add.student = name1;
    add.trainer = name2;
    (0, _fsfunc.addSiswa)(add);

  default:
    break;
}