"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addSiswa = exports.login = exports.register = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _promises = _interopRequireDefault(require("fs/promises"));

require("core-js/stable");

require("regenerator-runtime/runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var path = 'data.json';

var register = function register(karyawan) {
  _fs["default"].readFile(path, function (err, data) {
    if (err) console.log(err);
    var dataa = JSON.parse(data);
    var isAvailable = false;
    dataa.forEach(function (element) {
      if (element.name == karyawan.name) {
        console.log("Nama sudah terdaftar");
        isAvailable = true;
      }
    });

    if (isAvailable == false) {
      dataa.push(karyawan);

      _fs["default"].writeFile(path, JSON.stringify(dataa), {
        encoding: 'utf-8'
      }, function (err) {
        if (err) console.log(err);
        console.log("Berhasil register");
      });
    }
  });
};

exports.register = register;

var login = function login(data) {
  _promises["default"].readFile(path).then(function (data) {
    var dataa = JSON.parse(data);
    var searchData = dataa.findIndex(function (idx) {
      return idx.name == data.name && idx.password == data.password;
    });
    dataa[searchData].isLogin = true;
    return _promises["default"].writeFile(path, JSON.stringify(dataa));
  }).then(function () {
    return console.log("Berhasil Login");
  })["catch"](function (err) {
    console.log("Login Gagal!");
  });
};

exports.login = login;

var addSiswa = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(student, trainer) {
    var readdatas, dataa, searchData;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _promises["default"].readFile(path);

          case 3:
            readdatas = _context.sent;
            dataa = JSON.parse(readdatas);
            searchData = dataa.findIndex(function (item) {
              return item.name == trainer;
            });

            if (!(searchData == -1)) {
              _context.next = 10;
              break;
            }

            throw "User tidak ditemukan";

          case 10:
            if (dataa[searchData].isLogin) {
              _context.next = 12;
              break;
            }

            throw "User belum Login";

          case 12:
            if (!dataa[searchData].hasOwnProperty('students')) {
              dataa[searchData].students = [];
            }

            if (!(dataa[searchData].students.findIndex(function (item) {
              return item.name == student.name;
            }) != -1)) {
              _context.next = 15;
              break;
            }

            throw "Nama siswa telah terdaftar";

          case 15:
            dataa[searchData].students.push(student);
            _context.next = 18;
            return _promises["default"].writeFile(path, JSON.stringify(dataa));

          case 18:
            console.log("Berhasil add siswa");
            _context.next = 24;
            break;

          case 21:
            _context.prev = 21;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);

          case 24:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 21]]);
  }));

  return function addSiswa(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.addSiswa = addSiswa;