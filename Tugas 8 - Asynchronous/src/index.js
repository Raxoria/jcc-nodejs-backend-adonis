import {addSiswa, register} from './lib/fsfunc'
import {login} from './lib/fsfunc'


const args = process.argv.slice(2)
const command = args[0]

switch(command){
    case "register":
        let newData = {}
        let [nama,password,role] = args[1].split(',')
        newData.nama = nama
        newData.role = role
        newData.pass = password
        newData.isLogin = false
        register(newData);
        break
    case "login":
        let log = {}
        let [id,pass] = args[1].split(',')
        log.nama = id
        log.pass = pass
        login(log);
        break
    case "addSiswa":
        let add = {}
        let [name1,name2] = args[1].split(',')
        add.student = name1
        add.trainer = name2
        addSiswa(add);
    default:
        break
    }