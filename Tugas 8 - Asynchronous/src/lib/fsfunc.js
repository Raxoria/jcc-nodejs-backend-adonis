import fs from 'fs'
import fspromise from 'fs/promises'
import 'core-js/stable'
import 'regenerator-runtime/runtime'

const path = 'data.json'
export const register = (karyawan) => {
    fs.readFile(path, (err,data) => {
        if (err) console.log(err)
        let dataa = JSON.parse(data)
        var isAvailable = false
        dataa.forEach(element => {
            if(element.name == karyawan.name){
                console.log("Nama sudah terdaftar")
                isAvailable = true
            }
        });
        if(isAvailable == false){
            dataa.push(karyawan)
            fs.writeFile(path, JSON.stringify(dataa), {encoding: 'utf-8'}, (err) => {
                if (err) console.log(err)
                console.log("Berhasil register")
            })
        }
    })
}

export const login = (data) =>{
    fspromise.readFile(path)
    .then(data =>{
        let dataa = JSON.parse(data)
        let searchData = dataa.findIndex(idx => (idx.name == data.name && idx.password == data.password))
        dataa[searchData].isLogin = true
        return fspromise.writeFile(path, JSON.stringify(dataa))
    })
    .then(() => console.log("Berhasil Login"))
    .catch(err => {
        console.log("Login Gagal!")
    })
}

export const addSiswa = async (student, trainer) =>{
    try {
        let readdatas = await fspromise.readFile(path)
        let dataa = JSON.parse(readdatas)
        let searchData = dataa.findIndex(item => item.name == trainer)
        if(searchData == -1)
            throw "User tidak ditemukan"
        else if(!dataa[searchData].isLogin)
            throw "User belum Login"
        if(!dataa[searchData].hasOwnProperty('students')){
            dataa[searchData].students = []    
        }
        if(dataa[searchData].students.findIndex(item => item.name == student.name) != -1)
            throw "Nama siswa telah terdaftar"
        dataa[searchData].students.push(student)
        await fspromise.writeFile(path, JSON.stringify(dataa))
        console.log("Berhasil add siswa")
    } catch (err) {
        console.log(err)
    }
    
}